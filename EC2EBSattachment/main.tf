#creating aws instance
  resource "aws_instance" "web" {
   ami             = "ami-0323c3dd2da7fb37d"
   instance_type   = "t2.micro"
   security_groups = ["${aws_security_group.web-sg.name}"]
   user_data = "${file("server-script.sh")}"
   
   tags = {
      Name = "web"
   }
}

resource "aws_security_group" "web-sg" {
   name = "web-sg"
   ingress {
      to_port     = 80
      from_port   = 80
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
   }

   egress {
      to_port     = 80
      from_port   = 80
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
   }  

   ingress {
      to_port     = 443
      from_port   = 443
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
   }

   egress {
      to_port     = 443
      from_port   = 443
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
   }
}

resource "aws_eip" "web_ip" {
   instance = "${aws_instance.web.id}"
}




#creating and attaching ebs volume
resource "aws_ebs_volume" "data-vol" {
 size = 10
 availability_zone = "us-east-1"
 tags = {
        Name = "data-volume"
 }

}

resource "aws_volume_attachment" "good-morning-vol" {
 device_name = "/dev/sdc"
 volume_id = "${aws_ebs_volume.data-vol.id}"
 instance_id = "${aws_instance.web.id}"
}